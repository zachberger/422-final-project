#!/bin/bash

# remove former jar file
rm -f code.jar

# remove .class files
rm -rf src/*.class
rm -rf tests/*.class

# compile project source
javac -classpath src/ src/RunEdgeConvert.java

# build jar
jar -cmvf src/manifest.txt code.jar -C src/ src/*.class

# cleanup class files
rm -rf src/*.class

# compile unit tests
javac -classpath junit-4.12.jar:hamcrest-core-1.3.jar:code.jar -sourcepath tests/ tests/TestRunner.java

# run unit tests
if java -classpath junit-4.12.jar:hamcrest-core-1.3.jar:code.jar:tests/ TestRunner
then
	echo "tests passed!"
else
	echo "tests failed!"
fi

# cleanup .class files
rm -rf tests/*.class
