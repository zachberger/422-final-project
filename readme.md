
# project readme

###Summary of issues addressed for benchmark #1
1. organized files into logical structure (src for source code etc.)
1. fixed odd dependency issues with code so that it would properly compile outside of windows/netbeans/eclipse
1. created manifest for ability to create jar files
1. created build script to simplify compilitation and execution of tests
1. TestRunner.java will fire off our testing using jUnit (EdgeConvertGUITest.java and EdgeConvertFileParserTest.java under "resources")


###Other issues
- That save function is "fucked" (even the comments say so).
- The line endings are all windows, which breaks in linux/unix.
- The encoding is wonky, does not compile with default UTF-8, or java8?
