#!/bin/bash

# attempt a build
./build.sh

# only run if build works
if [ -f code.jar ]
then
	java -jar code.jar &
else
    echo "check your errors..."
fi
