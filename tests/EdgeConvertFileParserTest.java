
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

// import bullshit that allows our syntax to become even more god damned rediculous
// now our comments can act as execution tools!  Yay!
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

// this is so we can access Asset methods directly without referencing the awesome namespace
import static org.junit.Assert.*;

public class EdgeConvertFileParserTest {

	protected EdgeConvertFileParser file;
	protected File rawFile;
	
	@Before // setup the environment for testing
	public void setUp() throws Exception {
		rawFile = new File(this.getClass().getResource("/Courses.edg").getFile());
		file = new EdgeConvertFileParser(rawFile);
	}

	@After // cleanup after our tests
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetEdgeFields() throws IOException {
		assertEquals(file.getEdgeFields().length,7);
	}
	
	@Test
	public void testGetEdgeFieldsGetName() throws IOException {
		EdgeField[] fields = file.getEdgeFields();
		String[] names = {"FacultyName","StudentName","CourseName","Grade","StudentSSN","Number","FacSSN"};
		for ( EdgeField field : fields ) {
			assertTrue(Arrays.asList(names).contains(field.getName()));
		}
	}

	@Test
	public void testGetEdgeTables() throws IOException {
		assertEquals(file.getEdgeTables().length,3);
	}
	
	@Test
	public void testGetEdgeTablesGetName() throws IOException {
		EdgeTable[] tables = file.getEdgeTables();
		String[] names = {"STUDENT","FACULTY","COURSES"};
		for ( EdgeTable table : tables ) {
			assertTrue(Arrays.asList(names).contains(table.getName()));
		}
	}

}
