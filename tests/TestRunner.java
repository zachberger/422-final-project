import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

// @link: http://agile.csc.ncsu.edu/SEMaterials/tutorials/junit/junit_tutorial_jazz.html#section1_0
// @link: http://junit.sourceforge.net/junit3.8.1/doc/cookbook/cookbook.htm

public class TestRunner {
	public static void main(String[] args) {
		testEdgeConvertFileParser();
		testEdgeConvertGUI();
	}

	// Edge Convert File Parser
	private static void testEdgeConvertFileParser() {
		Result result = JUnitCore.runClasses(EdgeConvertFileParserTest.class);
		for (Failure failure : result.getFailures()) {
			System.out.println(failure.toString());
		}
		System.out.println(result.wasSuccessful());		
	}

	// Edge Convert GUI
	private static void testEdgeConvertGUI() {
		Result result = JUnitCore.runClasses(EdgeConvertGUITest.class);
		for (Failure failure : result.getFailures()) {
			System.out.println(failure.toString());
		}
		System.out.println(result.wasSuccessful());
	}
}
